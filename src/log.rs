use wlroots_sys::*;

pub enum Importance {
    Silent,
    Error,
    Info,
    Debug,
}

pub fn init(importance: Importance) {
    let importance = match importance {
        Importance::Silent => wlr_log_importance_WLR_SILENT,
        Importance::Error => wlr_log_importance_WLR_ERROR,
        Importance::Info => wlr_log_importance_WLR_INFO,
        Importance::Debug => wlr_log_importance_WLR_DEBUG,
    };
    unsafe {
        wlr_log_init(importance, None);
    }
}
