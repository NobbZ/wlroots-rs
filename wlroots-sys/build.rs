extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=src/wrapper.h");

    let wlroots_lib = pkg_config::Config::new()
        .range_version("0.15.0".."0.16.0")
        .probe("wlroots")
        .unwrap();

    let mut builder = bindgen::builder()
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .header("src/wrapper.h")
        .allowlist_type("wlr_.*")
        .allowlist_function("wlr_.*")
        .blocklist_type("wl_array")
        .blocklist_type("wl_client")
        .blocklist_type("wl_display")
        .blocklist_type("wl_event_loop")
        .blocklist_type("wl_event_source")
        .blocklist_type("wl_global")
        .blocklist_type("wl_list")
        .blocklist_type("wl_listener")
        .blocklist_type("wl_notify_func_t")
        .blocklist_type("wl_resource")
        .blocklist_type("wl_shm_buffer")
        .blocklist_type("wl_signal")
        .clang_arg("-DWLR_USE_UNSTABLE");

    for path_buf in wlroots_lib.include_paths {
        builder = builder.clang_arg(format!("-I{}", path_buf.to_str().unwrap()));
    }

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    builder
        .generate()
        .unwrap()
        .write_to_file(out_path.join("bindings.rs"))
        .unwrap();
}
